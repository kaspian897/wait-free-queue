package app;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class Node {
	String value;
	AtomicReference<Node> next;
	int enqTid;
	AtomicInteger deqTid;

	Node(String val, int etid) {
		value = val;
		next = new AtomicReference<Node>(null);
		enqTid = etid;
		deqTid = new AtomicInteger(-1);
	}
}
