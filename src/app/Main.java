package app;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Main {
	public static class Consumer extends Thread {
		private int TID;
		private WaitFreeQueue waitFreeQueue;

		public Consumer(int TID, WaitFreeQueue waitFreeQueue) {
			this.TID = TID;
			this.waitFreeQueue = waitFreeQueue;

		}

		public void run() {
			for(int i=0;i<1000000;i++) {
				try {
					waitFreeQueue.deq(TID);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					System.out.println("Pusta kolejka!!!!");
				}
			}
		}
	}

	public static class Producer extends Thread {
		private int TID;
		private WaitFreeQueue waitFreeQueue;

		public Producer(int TID, WaitFreeQueue waitFreeQueue) {
			this.TID = TID;
			this.waitFreeQueue = waitFreeQueue;
		}

		public void run() {
			Random random=new Random();
			for(int i=0;i<1000000;i++) {

				waitFreeQueue.enq(TID,random.nextInt(100000)+"");

			}
		}

	}

	public static void main(String[] args) {
		int numThread = 16;
		WaitFreeQueue waitFreeQueue = new WaitFreeQueue(numThread);
		Thread[] watki = new Thread[numThread];
		
		long start=System.currentTimeMillis();
		
		//Test enqueue-dequeue
		for (int i = 0; i < numThread; i++) {
			final int tid=i;
			watki[i] = new	Thread() {
				public void run() {
					WaitFreeQueue queue=waitFreeQueue;
					Random random=new Random();
					for(int j=0;j<1000000;j++) {
						queue.enq(tid, random.nextInt()+"");
						try {
							queue.deq(tid);
						} catch (Exception e) {
							//System.out.println("Pusta kolejka!!!!");
						}
					}
				}
			};
			watki[i].start();
		}
		for (int i = 0; i < numThread; i++) {
			try {
				watki[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		long stop=System.currentTimeMillis();
		System.out.println("TEST_1 Czas wykonania: "+(stop-start)+"ms");
		// test 
		start=System.currentTimeMillis();
		Random random=new Random();
		WaitFreeQueue waitFreeQueue2 = new WaitFreeQueue(numThread);
		for(int i=0;i<1000;i++) {
			waitFreeQueue.enq(0, random.nextInt()+"");
		}
		for (int i = 0; i < numThread; i++) {
			final int tid=i;
			watki[i] = new	Thread() {
				public void run() {
					WaitFreeQueue queue=waitFreeQueue2;
					Random random=new Random();
					for(int j=0;j<1000000;j++) {
						if(random.nextInt(2)%2==0)
						queue.enq(tid, random.nextInt()+"");
						else {
							try {
								queue.deq(tid);
							} catch (Exception e) {
								//System.out.println("Pusta kolejka!!!!");
							}
						}
					}
				}
			};
			watki[i].start();
		}
		for (int i = 0; i < numThread; i++) {
			try {
				watki[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		stop=System.currentTimeMillis();
		System.out.println("TEST_2 Czas wykonania: "+(stop-start)+"ms");
		// Zakomentowany kod do testowania og�lnego kolejki
		

		Scanner scanner = new Scanner(System.in);
		System.out.print("podaj liczbe Producerow: ");
		int numProducer = scanner.nextInt();
		System.out.print("podaj liczbe Konsumentow: ");
		int numConsumer = scanner.nextInt();
		numThread = numProducer + numConsumer;
		start=System.currentTimeMillis();
		WaitFreeQueue waitFreeQueue3 = new WaitFreeQueue(numThread);
		watki = new Thread[numThread];
		for (int i = 0; i < numProducer; i++) {
			watki[i] = new Producer(i, waitFreeQueue3);
			watki[i].start();
		}
		for (int i = numProducer; i < numProducer + numConsumer; i++) {
			watki[i] = new Consumer(i, waitFreeQueue3);
			watki[i].start();
		}

		for (int i = 0; i < numThread; i++) {
			try {
				watki[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		stop=System.currentTimeMillis();
		System.out.println("TEST_3 Czas wykonania: "+(stop-start)+"ms");
		System.out.print("Koniec programu");
	}
}
