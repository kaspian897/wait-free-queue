package app;

public class OpDesc {
	long phase;
	boolean pending;
	boolean enqueue;
	Node node;

	OpDesc(long ph, boolean pend, boolean enq, Node n) {
		phase = ph;
		pending = pend;
		enqueue = enq;
		node = n;
	}
}